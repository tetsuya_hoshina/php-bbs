<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

# Users系の処理を呼び出す用
Route::resource('users', 'UsersController');
// 会員削除確認画面のルート設定
Route::get('users','UsersController@delete_confirm')->name('users.delete_confirm');
# Messages系の処理を呼び出す用
Route::resource('messages', 'MessagesController');
# Comments系の処理を呼び出す用
Route::resource('comments', 'CommentsController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');