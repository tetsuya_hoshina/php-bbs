<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

# CommentModelを使用できるように定義
use App\Comment;
# MessageModelを使用できるように定義
use App\Message;
use Auth;

class CommentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * コメントの登録
     */
    public function store(Request $request)
    {
        // バリデーションの実装
	    $request->validate([
	        'text' => 'required'
	    ]);

        // フォームの入力値を取得
        $inputs = $request->input();

        // 認証情報からユーザーIDの取得
        $inputs['user_id'] = Auth::user()->id;

        // コメントの登録処理
        Comment::create($inputs);

        // ホーム画面へリダイレクト
        return redirect('home')->with('success', $request->input('message_title').' にコメントしました');
    }

    /**
     * コメント削除処理
     */
     public function destroy($id){
    	$comment = Comment::find($id);
        // ユーザー認証処理
        if (Auth()->user()->id != $comment->user_id) {
            return redirect(route('home'))->with('errors', '許可されていない操作です');
        }

        $comment->delete();

        return redirect('home')->with('success', $comment->text.' の削除が成功しました');
	}

	/**
     * コメント編集画面を表示
     */
    public function edit($id){
    	$comment = Comment::find($id);
    	$message_id = $comment->message_id;
    	$message = Message::find($message_id);

    	// ユーザー認証処理
    	if (Auth::user()->id != $comment->user_id) {
    		return redirect(route('home'))->with('error', '許可されていない操作です');
    	}

        return view('comments.edit', compact('comment', 'message'));
	}

	/**
     * コメント編集処理
     */
     public function update(Request $request, $id){
     	// バリデーション
		$request->validate([
			'text' => 'required'
		]);

		$comment = Comment::find($id);
		$message_id = $comment->message_id;
    	$message = Message::find($message_id);

		// ユーザー認証処理
		if (Auth()->user()->id != $comment->user_id) {
			return redirect(route('home'))->with('error', '許可されていない操作です');
		}

		$comment->text = $request->input('text');
		$comment->save();

		return redirect('home')->with('success', $message->title.' へのコメント更新が成功しました');
	}

}