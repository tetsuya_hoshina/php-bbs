<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

# MessageModelを使用できるように定義
use App\Message;
use Auth;

class MessagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 投稿画面を表示
     */
    public function create()
    {
        return view('messages.create');
    }

    /**
     * 投稿の登録
     */
    public function store(Request $request)
	{
	    // バリデーションの実装
	    $request->validate([
	        'title' => 'required|max:10',
	        'text' => 'required'
	    ]);

	    // フォームの入力値を取得
	    $inputs = \Request::all();
	    $message_title = $request->input('title');

	    // 認証情報からユーザーIDの取得
	    $inputs['user_id'] = Auth::user()->id;

	    // 投稿の登録処理
	    Message::create($inputs);

	    // ホーム画面へリダイレクト
	    return redirect('home')->with('success', $message_title.' を新たに投稿しました');
	}
	
	/**
     * 投稿編集画面を表示
     */
    public function edit($id){
    	$message = Message::find($id);
    	
    	// ユーザー認証処理
    	if (Auth::user()->id != $message->user_id) {
    		return redirect(route('home'))->with('error', '許可されていない操作です');
    	}
        
        return view('messages.edit', compact('message'));
	}
	
	/**
     * 投稿編集処理
     */
     public function update(Request $request, $id){
     	// バリデーション
		$request->validate([
			'title' => 'required|max:10',
			'text' => 'required'
		]);
		
		$message = Message::find($id);
		// ユーザー認証処理
		if (Auth()->user()->id != $message->user_id) {
			return redirect(route('home'))->with('error', '許可されていない操作です');
		}
		
		$message->title = $request->input('title');
		$message->text = $request->input('text');
		$message->save();
		
		return redirect('home')->with('success', $request->input('title').' の更新が成功しました');
	}
	
	/**
     * 投稿削除処理
     */
     public function destroy($id){
    	$message = Message::find($id);
    	$message_title = $message->title;
        // ユーザー認証処理
        if (Auth()->user()->id != $message->user_id) {
            return redirect(route('home'))->with('errors', '許可されていない操作です');
        }
 
        $message->delete();
        
        return redirect('home')->with('success', $message_title.' の削除が成功しました');
	}
}