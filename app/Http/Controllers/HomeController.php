<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// MessageModelを使用できるように定義
use App\Message;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        // Messageの一覧を取得
        // $messages = Message::all();
        // idの降順でページネートで取得
        $messages = Message::orderBy('id', 'desc')->paginate(3);
        // 検索フォームで入力された値を取得する
        $search = $request->input('search');
        // クエリビルダ
        $query = Message::query();
        // もし検索フォームの入力の有無をチェック
        if ($search) {
            // 全角スペースを半角に変換
            $spaceConversion = mb_convert_kana($search, 's');
            // 単語を半角スペースで区切り、配列にする
            $wordArraySearched = preg_split('/[\s,]+/', $spaceConversion, -1, PREG_SPLIT_NO_EMPTY);
            // 単語をループで回し、ユーザーネームと部分一致するものがあれば、$queryとして保持される
            foreach($wordArraySearched as $value) {
                $query->where('title', 'like', '%'.$value.'%');
            }
            // 上記で取得した$queryをページネートにし、変数$usersに代入
            $messages = $query->paginate(3);
        }
        // ホーム画面でmessagesの変数を使えるように渡してあげる
        return view('home', compact('messages', 'search'));
    }
}