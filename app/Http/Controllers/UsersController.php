<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

# UserModelを使用できるように定義
use App\User;
use Auth;

class UsersController extends Controller
{
    /**
     * ユーザーの登録画面を表示
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * ユーザー情報の登録
     */
    public function store(Request $request)
    {
        // バリデーションの実装
	    $request->validate([
	        'name' => 'required|max:30',
	        'email' => 'required',
	        'password' => 'required',
	        'password_confirmation' => 'required'
	    ]);
        // フォームの入力値を取得
        $inputs = \Request::all();
        // パスワードのハッシュ化
        $inputs['password'] = Hash::make($inputs['password']);
        // ユーザーの登録処理
        User::create($inputs);
        // ログイン画面へリダイレクト
        return redirect('login');
    }

    /**
     * ユーザー編集画面を表示
     */
    public function edit($id){
    	$user = User::find($id);
    	// ユーザー認証処理
    	if (Auth::user()->id != $user->id) {
    		return redirect(route('home'))->with('error', '許可されていない操作です');
    	}
        return view('users.edit', compact('user'));
	}

	/**
     * ユーザー編集処理
     */
     public function update(Request $request, $id){
     	// バリデーションの実装
	    $request->validate([
	        'name' => 'required|max:30',
	        'email' => 'required',
	        'password' => 'required',
	        'password_confirmation' => 'required'
	    ]);
		$user = User::find($id);
		// ユーザー認証処理
		if (Auth()->user()->id != $user->id) {
			return redirect(route('home'))->with('error', '許可されていない操作です');
		}
		// 既存パスワードと異なるパスワードの場合は、ユーザー編集画面へリダイレクトする
		if (Hash::check($request->password, $user->password)) {
            $user->password = Hash::make($request->password);
        }else{
	        return redirect(route('users.edit', $user->id))->with('error', 'パスワードが間違っています');
        }
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->save();
		return redirect('home')->with('success', 'ユーザーネーム '.$user->name.' の情報更新が成功しました');
	}

    /**
     * ユーザー情報削除処理
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        // return redirect('/login');
        return redirect('/login')->with('success', '会員情報を削除しました');
    }

    /**
     * ユーザー情報削除確認画面の表示
     */
    public function delete_confirm()
    {
        return view('users.delete_confirm');
    }
}