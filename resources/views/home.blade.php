@extends('layout')

<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
<style type="text/css">
	#scrollTop {
		position: fixed;
		right: 50px;
		bottom: 50px;
		background-color: #DCDCDC;
		width: 50px;
  		height: 50px;
  		border-radius: 50%;
		color: gray;
  		line-height: 50px;
  		text-align: center;
		display: none;
	}
	#scrollTop:hover {
  		cursor: pointer;
  		opacity: 0.7;
	}
</style>
<title>ホーム</title>

@section('content')
<div class="container">
  <section class="section">
    <div class="column is-8 is-offset-2">

    <!-- 検索フォーム部 -->
    <form method="GET" action="{{ route('home') }}">
      <input type="search" class="input column is-8 is-offset-2" placeholder="検索ワードを入力" name="search" value="@if (isset($search)) {{ $search }} @endif">
      <div class="column is-8 is-offset-2 d-flex justify-content-center">
          <button type="submit" class="btn btn-primary mr-4">検索</button>
          <button class="btn btn-secondary">
              <a href="{{ route('home') }}" class="text-white">
                クリア
              </a>
          </button>
      </div>
    </form>

    <!-- フラッシュメッセージ部 -->
    @include('flashMessage')

      @foreach($messages as $message)
        <div class="box media">
          <figure class="media-left">
            <i class="fas fa-clipboard-list"></i>
          </figure>
          <div class="media-content">
            <div class="content">
              <p>
                <strong>{{ $message->title }}</strong>
              </p>
              <p>
                {{ $message->text }}
              </p>
              <p class="content-option-space">
                {{ $message->user->name }} / {{ $message->created_at }}
              </p>

              <!-- 投稿ボタン部 -->
              <!-- ユーザー認証処理 -->
              @if (!Auth::guest() && Auth::user()->id == $message->user_id)
                <div class="d-flex justify-content-end">
                  <!-- 編集ボタン -->
                  <div class="mr-2">
                    {!! Form::model($message, ['route' => ['messages.edit', $message->id], 'method' => 'get']) !!}
                    @csrf
                      {!! Form::submit('編集', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                  </div>
                  <!-- 削除ボタン -->
                  <div id="delete-btn-{{$message->id}}">
                    {!! Form::model($message, ['route' => ['messages.destroy', $message->id], 'method' => 'delete']) !!}
                    @csrf
                    @method('DELETE')
                      {!! Form::submit('削除', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                  </div>
                </div>
              @endif
            </div>
          </div>
        </div>

        @foreach($message->comments as $comment)
          <div class="column is-8 is-offset-4">
            <div class="box media">
              <figure class="media-left">
                <i class="fas fa-grin-beam"></i>
              </figure>
              <div class="media-content">
                <div class="content">
                  <p>
                    {{ $comment->text }}
                  </p>
                  <p class="content-option-space">
                    {{ $comment->user->name }} / {{ $comment->created_at }}
                  </p>

                  <!-- コメントボタン部 -->
                  <!-- ユーザー認証処理 -->
                  @if (!Auth::guest() && Auth::user()->id == $comment->user_id)
                    <div class="d-flex justify-content-end">
                      <!-- 編集ボタン -->
                      <div class="mr-2">
                        {!! Form::model($comment, ['route' => ['comments.edit', $comment->id], 'method' => 'get']) !!}
                        @csrf
                          {!! Form::submit('編集', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                      </div>
                      <!-- 削除ボタン -->
                      <div id="delete-btn-{{$comment->id}}">
                        {!! Form::model($comment, ['route' => ['comments.destroy', $comment->id], 'method' => 'delete']) !!}
                        @csrf
                        @method('DELETE')
                          {!! Form::submit('削除', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                      </div>
                    </div>
                  @endif
                </div>
              </div>
            </div>
          </div>
        @endforeach

        {!! Form::open(['url' => '/comments']) !!}
          <!-- hidden属性でメッセージIDとメッセージタイトルをセット -->
          {!! Form::hidden('message_id', $message->id) !!}
          {!! Form::hidden('message_title', $message->title) !!}
          <div class="column is-6 is-offset-6">
            <div class="field">
              <label for='label'>
                コメント :
              </label>
              <p class="control">
                {!! Form::textarea('text', null, ['class' => 'input is-medium', 'style' => 'height: 100px;']) !!}
              </p>
            </div>
            <div class="field">
              <p class="control" style="text-align: right;">
                {!! Form::submit('コメント投稿', ['class' => 'button is-primary is-medium']) !!}
              </p>
            </div>
          </div>
        {!! Form::close() !!}
      @endforeach
    </div>
    <!-- ページングリンク部 -->
    <div class="d-flex justify-content-center">
      {{ $messages->appends(request()->input())->links() }}
    </div>
	<!-- 画面上部へのスクロールボタン部 -->
	<div id="scrollTop">TOPへ</div>
  </section>
</div>
<script src="{{ asset('/js/delete.js') }}"></script>
<script src="{{ asset('/js/success.js') }}"></script>
<script src="{{ asset('/js/scrollTop.js') }}"></script>
@endsection