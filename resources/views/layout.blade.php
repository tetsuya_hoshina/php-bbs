<!DOCTYPE HTML>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>Laravel掲示板</title>

    <script src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" defer="defer"></script>
    <script src="/js/app.js" defer></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">
    <link href="/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <!-- ログインをしていないときのナビメニュー -->
                    @guest
                        <li class="nav-item">
                            {!! link_to("home", 'ホーム画面', ['class' => "text-light btn btn-info d-flex align-items-center mr-3"]) !!}
                        </li>
                        
                        <li class="nav-item">
                            {!! link_to("users/create", 'ユーザー登録', ['class' => "text-light btn btn-info d-flex align-items-center mr-3"]) !!}
                        </li>
                    @else
                    <!-- ログインをしているときのナビメニュー -->
                        <a class="navbar-brand text-light btn btn-info d-flex align-items-center mr-3" href="{{ url('home') }}">
                            ホーム画面
                        </a>
                        <a class="navbar-brand text-light btn btn-info d-flex align-items-center mr-3" href="{{ url('messages/create') }}">
                            新規投稿
                        </a>
                        <li class="nav-item dropdown d-flex align-items-center btn btn-secondary btn-sm">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <!-- ログアウトリンク -->
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    ログアウト
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                
                                <!-- ユーザー編集リンク -->
                                <a class="dropdown-item" href="{{ route('users.edit', Auth::user()->id) }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('user-edit-form').submit();">
                                    ユーザー設定
                                </a>
                                
                                <form id="user-edit-form" action="{{ route('users.edit', Auth::user()->id) }}" method="GET" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    @yield('content')

</body>
</html>