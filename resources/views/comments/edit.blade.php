@extends('layout')

<title>コメント編集画面</title>

@section('content')

<div class="container">
  <section class="section">
    <div class="column is-4 is-offset-4">
      <h1 class="title is-2 has-text-centered has-text-grey">コメント編集</h1>

      @include('flashMessage')
      
      {!! Form::model($comment, ['route'=>['comments.update', $comment->id]]) !!}
      @csrf
      @method('PUT')
        <div class="box">

          <div>
          	投稿タイトル
          </div>
          <div class="mb-3">
          	<strong>{{ $message->title }}</strong>
          </div>
          
          <div>
          	投稿本文
          </div>
          <div class="mb-3">
          	<strong>{{ $message->text }}</strong>
          </div>
          
          <div class="field">
            <label for='label'>
              コメント本文 :
            </label>
            <p class="control">
              {!! Form::textarea('text', null, ['class' => 'input is-medium', 'style' => 'height: 100px;']) !!}
            </p>
          </div>

          <br>
          <div class="field">
            <p class="control" style="width:100%">
              {!! Form::submit('更新', ['class' => 'button is-primary is-medium', 'style' => 'width:100%']) !!}
            </p>
          </div>
        </div>
      {!! Form::close() !!}
    </div>
  </section>
</div>

@endsection