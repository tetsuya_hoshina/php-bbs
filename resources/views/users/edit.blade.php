@extends('layout')

<title>ユーザー編集</title>

@section('content')

<div class="container">
  <section class="section">
    <div class="column is-4 is-offset-4">
      <h1 class="title is-2 has-text-centered has-text-grey">ユーザー編集</h1>

      @include('flashMessage')

      {!! Form::model($user, ['route'=>['users.update', $user->id]]) !!}
      @csrf
      @method('PUT')
        <div class="box">
          <div class="field">
            <label for='label'>
              名前 :
            </label>
            <p class="control">
              {!! Form::text('name', null, ['class' => 'input is-medium']) !!}
            </p>
          </div>

          <div class="field">
            <label for='label'>
              メールアドレス :
            </label>
            <p class="control">
              {!! Form::text('email', null, ['class' => 'input is-medium']) !!}
            </p>
          </div>

          <div class="field">
            <label for='label'>
              パスワード :
            </label>
            <p class="control">
              {!! Form::password('password', ['class' => 'input is-medium']) !!}
            </p>
          </div>

          <div class="field">
            <label for='label'>
              パスワード確認 :
            </label>
            <p class="control">
              {!! Form::password('password_confirmation', ['class' => 'input is-medium']) !!}
            </p>
          </div>

          <br>
          <div class="field">
            <p class="control" style="width:100%">
              {!! Form::submit('更新', ['class' => 'button is-primary is-medium', 'style' => 'width:100%']) !!}
            </p>
          </div>
          <div class="field">
            <button type="button" class="button is-danger is-medium w-100" onclick="location.href='http://127.0.0.1:8000/users'">会員退会画面へ</button>
          </div>
        </div>
      {!! Form::close() !!}
    </div>
  </section>
</div>

@endsection