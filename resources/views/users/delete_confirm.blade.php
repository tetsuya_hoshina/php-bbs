@extends('layout')

<title>ユーザー削除確認</title>

@section('content')

<div class="container">
  <section class="section">
    <div class="card mb-3">
      <div class="card-header d-flex justify-content-center">
        <div class="font-weight-bold h1 my-2">退会の確認</div>
      </div>
      <div class="card-body d-flex justify-content-center">
        <span class="card-text">退会をすると投稿も全て削除されます。</span>
        <span class="card-text">それでも退会をしますか？</span>
      </div>
    </div>

    <div class="btn-group d-flex justify-content-center">
      {!! Form::open(['route'=>['users.destroy',Auth::user()->id],'method'=>'delete']) !!}
        {!!Form::submit('退会する',['class'=>'button is-danger is-medium'])!!}
      {!!Form::close()!!}

      <div class="ml-3">
        <a href="/home" class="button is-info is-medium">キャンセルする</a>
      </div>
    </div>
  </section>
</div>

@endsection