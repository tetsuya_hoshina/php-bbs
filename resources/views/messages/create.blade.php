@extends('layout')

<title>新規投稿</title>

@section('content')

<div class="container">
  <section class="section">
    <div class="column is-4 is-offset-4">
      <h1 class="title is-2 has-text-centered has-text-grey">新規投稿</h1>

      @include('flashMessage')

      {!! Form::open(['url' => '/messages']) !!}
        <div class="box">
          <div class="field">
            <label for='label'>
              タイトル :
            </label>
            <p class="control">
              {!! Form::text('title', null, ['id' => 'title', 'class' => 'input is-medium', 'onchange' => 'isEnabledButton()']) !!}
            </p>
          </div>

          <div class="field">
            <label for='label'>
              本文 :
            </label>
            <p class="control">
              {!! Form::textarea('text', null, ['id' => 'textContents', 'class' => 'input is-medium', 'style' => 'height: 100px;', 'onchange' => 'isEnabledButton()']) !!}
            </p>
          </div>

          <br>
          <div class="field">
            <p class="control" style="width:100%">
              {!! Form::submit('投稿', ['id' => 'postButton', 'class' => 'button is-primary is-medium', 'style' => 'width:100%', 'disabled' => 'true']) !!}
            </p>
          </div>
        </div>
      {!! Form::close() !!}
    </div>
  </section>
</div>
<script src="{{ asset('/js/isEnabledButtonCreateMessage.js') }}"></script>
@endsection