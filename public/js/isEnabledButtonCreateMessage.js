function isEnabledButton() {
	let titleValue = document.getElementById('title').value;
	let titleContentsValue = document.getElementById('textContents').value;
	document.getElementById('postButton').disabled = !(titleValue && titleContentsValue);
}