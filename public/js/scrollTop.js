let scrollTopEle = document.getElementById('scrollTop');
// スクロールボタン表示処理
window.addEventListener('scroll', function() {
	let YPosition = window.pageYOffset;
	if (YPosition > 100) {
		scrollTopEle.style.display = 'block';
	} else {
		scrollTopEle.style.display = 'none';
	}
});
// 画面上部へのスクロール処理
scrollTopEle.addEventListener('click', () => {
	window.scrollTo({
		top: 0,
		behavior: 'smooth'
	});
});