(function() {
    'use strict';

    // 5秒後にサクセスメッセージの表示を消す
    $(function(){
        setTimeout (function () {
            $('.success-message').hide();
        }, 5000);
    });

})();