const DELETE_MESSAGE = "削除してもよろしいですか？"
// idセレクタを前方一致で指定している
$('[id^=delete-btn-]').click(function() {
	if(!confirm(DELETE_MESSAGE)){
		return false;
	}
});